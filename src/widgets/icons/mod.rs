mod group;
mod icon;

pub use self::{group::IconsGroupWidget, icon::IconWidget};
