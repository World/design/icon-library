pub static GRESOURCE_EXAMPLE: &str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<gresources>
  <gresource prefix=\"/org/gtk/example/icons/scalable/actions/\">
    <file preprocess=\"xml-stripblanks\">start-here-symbolic.svg</file>
  </gresource>
</gresources>";

pub fn gresource_example(icon_name: &str) -> String {
    format!(
        "<gresource prefix=\"/org/gtk/example/icons/scalable/actions/\">
  <file preprocess=\"xml-stripblanks\">{icon_name}.svg</file>
</gresource>",
    )
}

// Python
pub static PYTHON_MESON_EXAMPLE: &str = "gnome = import('gnome')
resources = gnome.compile_resources(
  'resources',
  'resources.gresource.xml',
  gresource_bundle: true,
  source_dir: meson.current_build_dir(),
  install: true,
  install_dir: get_option('datadir') / meson.project_name(),
)";
pub static PYTHON_EXAMPLE: &str = "import os
from gi.repository import Gio

if __name__ == \"__main__\":
    resource = Gio.resource_load(os.path.join('@PKGDATA_DIR@', 'resources.gresource'))
    Gio.Resource._register(resource)
";

// Rust
pub static RUST_MESON_EXAMPLE: &str = "gnome = import('gnome')
resources = gnome.compile_resources(
  'resources',
  'resources.gresource.xml',
  gresource_bundle: true,
  source_dir: meson.current_build_dir()
)";

pub static RUST_MAIN_EXAMPLE: &str = "fn main () {
    let res = gio::Resource::load(\"@PKGDATA_DIR@\".to_string() + \"/resources.gresource\").unwrap();
    gio::resources_register(&res);
}";

// Vala
pub static VALA_MESON_EXAMPLE: &str = "gnome = import('gnome')
resource_files = files('gnome-clocks.gresource.xml')
resources = gnome.compile_resources('resources',
  'resources.gresource.xml',
  c_name: 'resources'
)
# Include the resource in the binary
executable(meson.project_name(),
  [
    'main.vala',
    resources,
  ],
  install: true
)";

// JS
pub static JS_MESON_EXAMPLE: &str = "gnome = import('gnome')

gnome.compile_resources(
  'org.gnome.design.IconLibrary.data',
  'resources.data.gresource.xml',
  gresource_bundle: true,
  install_dir: get_option('datadir') / meson.project_name(),
  install: true
)";

pub static JS_EXAMPLE: &str = "imports.package.init({ name: \"org.gnome.design.IconLibrary\" });
imports.package.run(imports.app.main);";

// C
pub static C_MESON_EXAMPLE: &str = "gnome = import('gnome')
resources = gnome.compile_resources('resources',
  'resources.gresource.xml',
  source_dir: '.',
  c_name: 'resources'
)
executable(
  meson.project_name(),
  [
    'main.c',
    resources
  ],
  install: true
)";
