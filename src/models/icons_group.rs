use gtk::{gio, gio::prelude::*, glib, subclass::prelude::*};

use crate::models::Icon;

mod imp {
    use std::cell::{Cell, OnceCell};

    use super::*;

    #[derive(Debug, glib::Properties)]
    #[properties(wrapper_type = super::IconsGroup)]
    pub struct IconsGroup {
        #[property(get, set, construct_only)]
        pub name: OnceCell<String>,
        #[property(get)]
        pub icons: gio::ListStore,
        #[property(get, set)]
        pub is_system: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for IconsGroup {
        const NAME: &'static str = "IconsGroup";
        type Type = super::IconsGroup;

        fn new() -> Self {
            let icons = gio::ListStore::new::<Icon>();
            Self {
                icons,
                name: Default::default(),
                is_system: Cell::new(false),
            }
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for IconsGroup {}
}

glib::wrapper! {
    pub struct IconsGroup(ObjectSubclass<imp::IconsGroup>);
}

impl IconsGroup {
    pub fn new(name: &str) -> Self {
        glib::Object::builder().property("name", name).build()
    }

    pub fn add_icons(&self, icons: &[Icon]) {
        self.imp().icons.splice(0, 0, icons);
    }
}
