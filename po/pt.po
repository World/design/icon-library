# Portuguese translation for icon-library.
# Copyright (C) 2022 icon-library's COPYRIGHT HOLDER
# This file is distributed under the same license as the icon-library package.
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: icon-library master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/icon-library/"
"issues\n"
"POT-Creation-Date: 2022-03-22 17:35+0000\n"
"PO-Revision-Date: 2022-03-23 21:52+0000\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>\n"
"Language-Team: Portuguese <pt@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0.1\n"

#: data/org.gnome.design.IconLibrary.desktop.in.in:3
#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:7 src/application.rs:69
msgid "Icon Library"
msgstr "Biblioteca de ícones"

#: data/org.gnome.design.IconLibrary.desktop.in.in:4
msgid "Find the right icon to use on your GNOME application"
msgstr "Encontre o ícone correto para usar na sua aplicação GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.design.IconLibrary.desktop.in.in:10
msgid "Icon;Library;GNOME;GTK;"
msgstr "Icon;Ícone;Library;Biblioteca;GNOME;GTK;"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:6
msgid "Width of the last opened window"
msgstr "Largura da última janela aberta"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:10
msgid "Height of the last opened window"
msgstr "Altura da última janela aberta"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:14
msgid "Maximized state of the last opened window"
msgstr "Estado maximizado da última janela aberta"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:18
msgid "Enable or disable dark mode"
msgstr "Ativar ou desativar o modo escuro"

#: data/org.gnome.design.IconLibrary.gschema.xml.in:22
msgid "The default doc-page"
msgstr "A página de documentação predefinida"

#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:8 src/application.rs:72
msgid "Symbolic icons for your apps"
msgstr "Ícones simbólicos para as suas aplicações"

#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:10
msgid "Find the right icon to use on your GNOME application."
msgstr "Encontre o ícone correto para usar na sua aplicação GNOME."

#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Janela principal"

#: data/org.gnome.design.IconLibrary.metainfo.xml.in.in:101
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/resources/ui/export_dialog.ui:139
msgid "_Save As…"
msgstr "_Guardar como…"

#: data/resources/ui/export_dialog.ui:150
msgid "_Copy Name"
msgstr "_Copiar nome"

#: data/resources/ui/export_dialog.ui:167
msgid "Use in a Mockup"
msgstr "Usar num Mockup"

#: data/resources/ui/export_dialog.ui:170
msgid "_Copy to Clipboard"
msgstr "_Copiar para área de transferência"

#: data/resources/ui/export_dialog.ui:182
#: data/resources/ui/export_dialog.ui:224
msgid "Include in an App"
msgstr "Incluir numa aplicação"

#: data/resources/ui/export_dialog.ui:193
#: data/resources/ui/export_dialog.ui:734
msgid "Use as a System Icon"
msgstr "Use como um ícone do sistema"

#: data/resources/ui/export_dialog.ui:253
msgid ""
"In order to use a custom icon in your app, it needs to be shipped as a "
"GResource."
msgstr ""
"Para usar um ícone personalizado na sua aplicação, o mesmo precisa de ser "
"enviado como um GResource."

#: data/resources/ui/export_dialog.ui:262
msgid ""
"The first step is saving the file in your project directory (e.g. in <b>/"
"data/icons</b>)."
msgstr ""
"A primeira etapa é guardar o ficheiro no diretório do projeto (por exemplo, "
"em <b>/data/icons</b>)."

#: data/resources/ui/export_dialog.ui:272
msgid "_Save SVG to…"
msgstr "_Guardar SVG em…"

#: data/resources/ui/export_dialog.ui:284
msgid ""
"Then you need to include the file in your resource XML file, with a line "
"similar to this∶"
msgstr ""
"Depois precisa de incluir o ficheiro no ficheiro XML de recurso, com uma "
"linha semelhante a esta∶"

#: data/resources/ui/export_dialog.ui:317
msgid ""
"You should be able to use the icon by setting <i>icon-name</i> property of "
"<i>Gtk.Image</i>"
msgstr ""
"Deve poder usar o ícone ao definir a propriedade <i>icon-name</i> de <i>Gtk."
"Image</i>"

#: data/resources/ui/export_dialog.ui:329
msgid "No Resource File?"
msgstr "Nenhum ficheiro de recurso?"

#: data/resources/ui/export_dialog.ui:341
msgid ""
"If you don't have a resource file in your project yet, you can start from "
"this example file."
msgstr ""
"Se ainda não possui um ficheiro de recurso no seu projeto, pode começar "
"neste ficheiro de exemplo."

#: data/resources/ui/export_dialog.ui:350
msgid "Save _Resource File to…"
msgstr "Guardar ficheiro de _recurso em…"

#: data/resources/ui/export_dialog.ui:362
msgid ""
"<a href='https://docs.gtk.org/gio/struct.Resource.html'>Read more on "
"GResources</a>"
msgstr ""
"<a href='https://docs.gtk.org/gio/struct.Resource.html'>Saiba mais em "
"GResources</a>"

#: data/resources/ui/export_dialog.ui:374
msgid "Include the GResource in your app"
msgstr "Incluir o GResource na sua aplicação"

#: data/resources/ui/export_dialog.ui:406
#: data/resources/ui/export_dialog.ui:449
msgid ""
"In order for this to work you need to include it in your meson.build, like "
"so∶"
msgstr ""
"Para que isto funcione, precisa de incluí-lo no seu meson.build, da seguinte "
"forma∶"

#: data/resources/ui/export_dialog.ui:480
#: data/resources/ui/export_dialog.ui:632
msgid "And then load it in your main file"
msgstr "E depois carregá-lo no seu ficheiro principal"

#: data/resources/ui/export_dialog.ui:523
#: data/resources/ui/export_dialog.ui:599
msgid ""
"In order for this to work you need to create a bundle out of the "
"<b>GResource</b> file and install it in <b>$prefix/$datadir/$app-name</b>. "
"Here's how you would do it using Meson"
msgstr ""
"Para que isto funcione, precisa de criar um pacote fora do ficheiro <b> "
"GResource</b> e instalá-lo em <b>$prefix/$datadir/$app-name</b>. Eis como o "
"faria usando o Meson"

#: data/resources/ui/export_dialog.ui:555
msgid "And then load the bundle in your main file"
msgstr "E depois carregue o pacote no seu ficheiro principal"

#: data/resources/ui/export_dialog.ui:677
msgid ""
"In order for this to work you need to include the <b>GResource</b> in the "
"binary."
msgstr ""
"Para que isto funcione, precisa de incluir o <b>GResource</b> no binário."

#: data/resources/ui/export_dialog.ui:759
msgid ""
"This is a system icon, which means it comes pre-installed as part of the "
"platform."
msgstr ""
"Isto é um ícone do sistema, o que significa que vem pré-instalado como parte "
"da plataforma."

#: data/resources/ui/export_dialog.ui:768
msgid ""
"Note that system icons can change over time, so if you are using this icon "
"because of what it looks like rather than its semantic meaning it would be "
"better to <a href='in-app'>include it with your app</a>."
msgstr ""
"Observe que os ícones do sistema podem mudar com o tempo, portanto, se "
"estiver a usar este ícone por causa da sua aparência e não pelo significado "
"semântico, seria melhor <a href='in-app'>incluí-lo com a sua aplicação</a>."

#: data/resources/ui/export_dialog.ui:778
msgid "_Copy Icon Name"
msgstr "_Copiar nome do ícone"

#: data/resources/ui/icons_group.ui:26
msgid ""
"These icons come with the operating system, which means you have no control "
"over them and they could change at any time. Only use them if you're sure "
"you are using them in the correct context (e.g. three lines for a primary "
"menu)."
msgstr ""
"Esses ícones vêm com o sistema operativo, o que significa que não tem "
"controlo sobre eles e os mesmos podem mudar a qualquer momento. Use-os "
"apenas se tiver a certeza de que os está a usar no contexto correto (por "
"exemplo, três linhas para um menu principal)."

#: data/resources/ui/shortcuts.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Geral"

#: data/resources/ui/shortcuts.ui:17
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Mostrar atalhos"

#: data/resources/ui/shortcuts.ui:24
msgctxt "shortcut window"
msgid "Quit"
msgstr "Sair"

#: data/resources/ui/window.ui:5
msgid "_Keyboard Shortcuts"
msgstr "_Teclas de atalho"

#: data/resources/ui/window.ui:9
msgid "About Icon Library"
msgstr "_Acerca da Biblioteca de ícones"

#: data/resources/ui/window.ui:29
msgid "Search for icons by name, category or tag"
msgstr "Pesquisar ícones por nome, categoria ou etiqueta"

#: data/resources/ui/window.ui:41
msgid "Toggle Dark Mode"
msgstr "Modo escuro"

#: data/resources/ui/window.ui:95
msgid "No Results found"
msgstr "Nenhum resultado encontrado"

#: data/resources/ui/window.ui:96
msgid "Try a different search"
msgstr "Tente uma pesquisa diferente"

#: src/application.rs:76
msgid "translator-credits"
msgstr "Hugo Carvalho <hugokarvalho@hotmail.com>"

#: src/widgets/export.rs:261 src/widgets/export.rs:368
msgid "Copied"
msgstr "Copiado"

#: src/widgets/export.rs:285
msgid "Export GResource example file"
msgstr "Exportar ficheiro de exemplo de GResource"

#: src/widgets/export.rs:288 src/widgets/export.rs:322
msgid "Export"
msgstr "Exportar"

#: src/widgets/export.rs:289 src/widgets/export.rs:323
msgid "Cancel"
msgstr "Cancelar"

#: src/widgets/export.rs:293
msgid "XML Document"
msgstr "Documento XML"

#: src/widgets/export.rs:319
msgid "Export a symbolic icon"
msgstr "Exportar um ícone simbólico"

#: src/widgets/export.rs:327
msgid "SVG images"
msgstr "Imagens SVG"
